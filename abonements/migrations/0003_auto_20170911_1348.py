# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-09-11 10:48
from __future__ import unicode_literals

from django.db import migrations
import djangocms_text_ckeditor.fields
import shop.money.fields


class Migration(migrations.Migration):

    dependencies = [
        ('abonements', '0002_abonement'),
    ]

    operations = [
        migrations.AlterField(
            model_name='abonement',
            name='description',
            field=djangocms_text_ckeditor.fields.HTMLField(default='Description', help_text='Description', verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='abonement',
            name='unit_price',
            field=shop.money.fields.MoneyField(decimal_places=1, verbose_name='Price'),
        ),
    ]
