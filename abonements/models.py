# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date
from django.db import models
from django.utils.translation import ugettext_lazy as _
from filer.fields.image import FilerImageField

from djangocms_text_ckeditor.fields import HTMLField
from shop.money.fields import MoneyField
from myshop.models import Product

class AbonementGroup(models.Model):
    name = models.CharField(
        _("Group name"),
        max_length = 255
    )

    image = FilerImageField(
        verbose_name=_('Image'), 
        blank=True, 
        null=True,
        on_delete=models.SET_NULL)

    description = HTMLField(
        _("Description"), default='Description',
        configuration='CKEDITOR_SETTINGS_DESCRIPTION',
        help_text=_("Description"),
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Abonement group')
        verbose_name_plural = _('Abonement groups')

class Abonement(Product):

    product_code = models.CharField(_("Product code"), max_length=255, null=True)

    unit_price = MoneyField(
        _("Price"),
        decimal_places=0,
    )

    description = HTMLField(
        verbose_name=_("Description"),
        configuration='CKEDITOR_SETTINGS_DESCRIPTION',
        null=True,
        blank=True
    )

    group = models.ForeignKey(
        AbonementGroup,
        on_delete=models.CASCADE
    )
    def save(self, *args, **kwargs):
        if not self.pk:
            self.product_code = "A{}".format( Product.objects.latest('pk').pk+1)
        super(Abonement, self).save(*args, **kwargs)

    def __str__(self):
        return self.product_name

    def get_price(self, request):
        return self.unit_price

    def get_absolute_url(self):
        return ''

    class Meta:
        app_label = 'abonements'
        ordering = ('order',)
        verbose_name = _('Abonement')
        verbose_name_plural = _('Abonements')