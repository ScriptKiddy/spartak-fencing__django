# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.utils.module_loading import import_string
from django.utils.translation import ugettext_lazy as _

from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar

class AbonementsToolbar(CMSToolbar):
    def populate(self):
        menu = self.toolbar.get_or_create_menu('abonements', _("Abonements"))

        abonement_menu = menu.get_or_create_menu(
            'abonement-menu',
            _('Abonements'),
        )

        url = reverse('admin:abonements_abonement_changelist')
        abonement_menu.add_sideframe_item(_('Abonements list'), url=url)

        url = reverse('admin:abonements_abonement_add')
        abonement_menu.add_modal_item(_('Add abonement'), url=url)

        group_menu = menu.get_or_create_menu(
            'group-menu',
            _('Groups'),
        )

        url = reverse('admin:abonements_abonementgroup_changelist')
        group_menu.add_sideframe_item(_('Groups list'), url=url)

        url = reverse('admin:abonements_abonementgroup_add')
        group_menu.add_modal_item(_('Add group'), url=url)

toolbar_pool.register(AbonementsToolbar)  