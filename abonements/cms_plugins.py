from django.forms import widgets
from django.utils.translation import ugettext_lazy as _

from cms.plugin_pool import plugin_pool
from entity_plugins.plugins_base import EntityPluginBase

from cmsplugin_cascade.plugin_base import CascadePluginBase
from cmsplugin_cascade.fields import GlossaryField

from .models import AbonementGroup, Abonement

class AbonementsGroupPlugin(EntityPluginBase):
    module = _("Abonements")
    name = _("Abonements group")
    entity_model = AbonementGroup
    render_template = "abonements/group.html"
    context_variable = 'group'
    cache = False
    color_scheme = GlossaryField(
        widgets.Select(choices=[('main', 'Main'), ('second', 'Second'), ('third', 'Thrid')]),
        label=_("Color scheme"),
    )

    def render(self, context, instance, placeholder):
        context = super(AbonementsGroupPlugin, self).render(context, instance, placeholder);
        try:
            context['color_scheme'] = instance.glossary['color_scheme']
        except Exception as e:
            pass

        try:
            group_id = instance.glossary['entity']['pk']
            context['abonements'] = Abonement.objects.filter(group=group_id)
        except Exception as e:
            pass
        return context  
plugin_pool.register_plugin(AbonementsGroupPlugin)
