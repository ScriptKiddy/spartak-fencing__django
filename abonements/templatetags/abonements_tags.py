from django import template
register = template.Library()
@register.filter(name='br_after')
def br_after(value, pos=None):
    if pos:
        arr = value.split(' ')
        arr.insert(pos, '<br>')
        return " ".join(arr)

    return value
