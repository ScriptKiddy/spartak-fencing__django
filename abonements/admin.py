from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from adminsortable2.admin import SortableAdminMixin
from .models import Abonement, AbonementGroup


class AbonementAdmin(SortableAdminMixin, admin.ModelAdmin):
    fieldsets = ((None, {
        'fields': ('product_name', 'unit_price',
                   'active', 'description', ),
    }), (_("Properties"), {
        'fields': ('group',)
    }), )
    list_display = ('product_name', 'group', 'unit_price', 'active', )
    list_filter = ('group__name',)

admin.site.register(Abonement, AbonementAdmin)

class AbonementGroupAdmin(admin.ModelAdmin):
    pass

admin.site.register(AbonementGroup, AbonementGroupAdmin)