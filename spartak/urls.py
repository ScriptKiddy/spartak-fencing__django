# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve

from django.views.generic import TemplateView
from myauth.views import PasswordResetConfirm
admin.autodiscover()

urlpatterns = [
    url(r'^sitemap\.xml$', sitemap,
        {'sitemaps': {'cmspages': CMSSitemap}}),
]

urlpatterns += [

    #DJANGO ADMIN
    url(r'^admin/', include(admin.site.urls)),

    #IORDER
    url(r'^iorder/', include('iorder.urls', namespace="iorder")),

    #MYAUTH
    url(r'^myauth/', include('myauth.urls', namespace="myauth")),
    url(r'^login/$', TemplateView.as_view(template_name='myauth/pages/login.html'), name="login-page"),
    
    #RESET PASSWORD CONFIRM PAGE
    url(r'^password-reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetConfirm.as_view(), name='password_reset_confirm'),

    #BOOKING
    url(r'^booking/', include('booking.urls', namespace="booking")),

    #GALLERY
    url(r'^gallery/', include('gallery.urls', namespace="gallery")),
    
    #PAYMENTS
    url(r'^fail-payment/$', TemplateView.as_view(template_name='myshop/payment/fail.html'), name='payment_fail'),
    url(r'^success-payment/$', TemplateView.as_view(template_name='myshop/payment/success.html'), name='payment_success'),
    url(r'^yandex-money/', include('yandex_money.urls')),
    url(r'^yandex-money/redirect/', include('shop_yandex.urls', namespace="shop-yandex")),

    #FEEDBACK
    url(r'^feedback/', include('feedback.urls', namespace="feedback")),

    #ORDERS-CONTROL
    url(r'^shop/', include('myshop.urls', namespace="shop")),
    #CMS PAGES
    url(r'^', include('cms.urls')),
    #prefix_default_language=False
]

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns
