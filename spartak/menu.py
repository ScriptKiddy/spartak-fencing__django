from django.utils.translation import ugettext_lazy as _

from django.urls import reverse
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from cms.menu_bases import CMSAttachMenu

class UserMenu(CMSAttachMenu):
    name = _("User menu")
    def get_nodes(self, request):
        nodes = []
        nodes.append(NavigationNode(_('My orders'), reverse('user-orders:list'), 1))
        nodes.append(NavigationNode(_('My profile'), reverse('user:profile'), 2))
        nodes.append(NavigationNode(_('Logout'), reverse('myauth:logout'), 3))
        return nodes

menu_pool.register_menu(UserMenu)