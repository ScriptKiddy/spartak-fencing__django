from django.contrib import admin
from email_auth.models import User

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass