from django.db import models
from django.utils.translation import ugettext_lazy as _

class Message(models.Model):
    name = models.CharField(
        _("Name"),
        max_length=255,
    )

    email = models.CharField(
        _("E-Mail"),
        max_length=255,
    )

    phone = models.CharField(
        _("Phone"),
        max_length=255,
        null=True,
        )

    text = models.TextField(
        _("Message Text"),
        max_length=1024
        )

    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} ({}) - {}".format( self.name, self.email, self.created.strftime("%d/%m/%y") ) 

    class Meta:
        verbose_name = _('Message')
        verbose_name_plural = _('Messages')

class CallRequest(models.Model):
    name = models.CharField(
        _("Name"),
        max_length=255,
    )

    email = models.CharField(
        _("E-Mail"),
        max_length=255,
    )

    phone = models.CharField(
        _("Phone"),
        max_length=255,
        null=True,
        )

    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} ({}) - {}".format( self.name, self.phone, self.created.strftime("%d/%m/%y") ) 

    class Meta:
        verbose_name = _('Call request')
        verbose_name_plural = _('Call requests')

class CampRequest(models.Model):
    name = models.CharField(
        _("Name"),
        max_length=255,
    )

    email = models.CharField(
        _("E-Mail"),
        max_length=255,
    )

    phone = models.CharField(
        _("Phone"),
        max_length=255,
        null=True,
        )

    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} ({}) - {}".format( self.name, self.email, self.created.strftime("%d/%m/%y") ) 

    class Meta:
        verbose_name = _('Camp request')
        verbose_name_plural = _('Camp requests')