# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import Message, CallRequest, CampRequest

class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

class CallRequestSerializer(ModelSerializer):
    class Meta:
        model = CallRequest
        fields = '__all__'

class CampRequestSerializer(ModelSerializer):
    class Meta:
        model = CampRequest
        fields = '__all__'