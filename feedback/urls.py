from django.conf.urls import url
from .views import  FeedbackView
from .serializers import MessageSerializer, CallRequestSerializer, CampRequestSerializer

urlpatterns = [
    url(r'^message/$', FeedbackView.as_view(serializer_class=MessageSerializer), name='message'),
    url(r'^call/$', FeedbackView.as_view(serializer_class=CallRequestSerializer), name="call"),
    url(r'^camp/$', FeedbackView.as_view(serializer_class=CampRequestSerializer), name="camp"),
]