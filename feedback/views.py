from django.utils.translation import ugettext_lazy as _
from rest_framework.generics import CreateAPIView
from rest_framework import status
from rest_framework.response import Response

from .serializers import MessageSerializer

class FeedbackView(CreateAPIView):
    throttle_scope = 'feedback'
    serializer_class = None
    success_message = _("Message sent successfully")
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer:
            return Response({"error": _('Configuration error')}, status=status.HTTP_400_BAD_REQUEST)

        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({"success": self.success_message}, status=status.HTTP_201_CREATED, headers=headers)
