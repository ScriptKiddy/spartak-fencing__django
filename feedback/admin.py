from django.contrib import admin
from .models import Message, CallRequest, CampRequest
admin.site.register(Message)
admin.site.register(CallRequest)
admin.site.register(CampRequest)