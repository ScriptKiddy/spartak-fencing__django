from django.utils.translation import ugettext_lazy as _
from cms.plugin_pool import plugin_pool
from cmsplugin_cascade.plugin_base import CascadePluginBase
from cmsplugin_cascade.fields import GlossaryField
from django.forms import widgets
class FeedbackPlugin(CascadePluginBase):
    module = "feedback"
    name = _("Feedback form")
    require_parent = False
    allow_children = False
    render_template = 'feedback/plugins/feedback-plugin.html'
    cache = False
    form_class  = GlossaryField(
        widgets.Select(choices=[('message', _('Message form')), ('camp', _('Camp form')), ('call', _('Call form'))]),
        label=_("Choose Feedback Form"),
    )    

plugin_pool.register_plugin(FeedbackPlugin)