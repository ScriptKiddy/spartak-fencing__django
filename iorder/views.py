from django.views.generic import TemplateView
from django.urls import reverse_lazy
from decimal import Decimal
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.generics import GenericAPIView
from rest_framework import status

from shop.modifiers.pool import cart_modifiers_pool
from shop.models.cart import CartModel, CartItemModel

from .serializers import ProductSerializer, IOrderSerializer

class IOrderView(GenericAPIView):
    serializer_class = IOrderSerializer
    retrive_serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        cart = CartModel.objects.get_from_request(request)
        cart.empty()
        cart.save()
        CartItemModel.objects.get_or_create(product = serializer.get_product_instance(), 
                                                        customer = request.customer, 
                                                        cart = cart,
                                                        extra = serializer.data.get('extra'),
                                                        quantity = serializer.data.get('quantity')) 
        pay_method = serializer.data.get('pay_method')
        for modifier in cart_modifiers_pool.get_payment_modifiers():
            if pay_method == modifier.identifier:
                payment_provider = getattr(modifier, 'payment_provider', None)
                if payment_provider:
                    expression = payment_provider.get_payment_request(cart, request)
                    return Response({'expression': expression}, status=status.HTTP_201_CREATED)
                break
        return Response({'error':'Something went wrong'}, status=status.HTTP_400_BAD_REQUEST)


    def retrieve_data(self, request, *args, **kwargs):
        quantity = request.GET.get('quantity', 1)
        product = request.GET.get('product', -1)  
        payments = request.GET.get('payments[]',[])
        serializer = self.retrive_serializer_class(context={'product':product, 'quantity': quantity, 'payments':payments, 'request': request})
        return serializer.data

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return Response(self.retrieve_data(request,args,kwargs), status=status.HTTP_202_ACCEPTED)
