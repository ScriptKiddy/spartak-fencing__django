from django.conf.urls import url
from .views import IOrderView

urlpatterns = [
    url(r'^$', IOrderView.as_view(), name='checkout'),
]