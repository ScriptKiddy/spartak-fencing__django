from django import template
from django.conf import settings
from django.utils.html import format_html
from classytags.core import Tag, Options
from classytags.helpers import InclusionTag
from classytags.arguments import Argument

register = template.Library()

class iorder_button(InclusionTag):
    """
    Inclusion tag for displaying checkout button
    """
    options = Options(
        Argument('product'),
        Argument('css'),
        Argument('caption')
    )
    template = "iorder/order-button.html"
    takes_context=True
    def get_context(self, context, product, css, caption):
        tag_context =  {
            'product':product, 
            'customer':context['request'].customer,
            'csrf': format_html(
                        "<input type='hidden' name='csrfmiddlewaretoken' value='{}' />",
                        context['csrf_token']
                    ), 
            'csrf_token': context['csrf_token'],
            'request': context['request'], 
            'SEKIZAI_CONTENT_HOLDER': context['SEKIZAI_CONTENT_HOLDER'],
            'STATIC_URL': context['STATIC_URL'],
            'css':css,
            'caption':caption
            }
        return tag_context
register.tag(iorder_button)