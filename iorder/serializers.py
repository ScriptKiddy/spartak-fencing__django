# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from shop.models.product import ProductModel

from shop.rest.money import MoneyField
from shop.modifiers.pool import cart_modifiers_pool

from rest_framework.fields import empty
from decimal import Decimal

from django.conf import settings

class ProductSerializer(serializers.Serializer):
    product = serializers.IntegerField()
    quantity = serializers.IntegerField(default=1, min_value=1)
    extra_rows = serializers.ListField(default=[])
    payments = serializers.DictField(read_only=True)
    
    product_code = serializers.CharField(read_only=True)
    product_name = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    subtotal = MoneyField(read_only=True)
    unit_price = MoneyField(read_only=True)


    def __init__(self, instance=None, data=empty, **kwargs):
        if 'context' in kwargs:
            context = kwargs.get('context')
            instance = self.get_instance(context)
            instance.setdefault('subtotal', Decimal(instance['quantity']) * instance['unit_price'])
            super(ProductSerializer, self).__init__(instance, data, context=context)
        else:
            super(ProductSerializer, self).__init__(instance, data, **kwargs)

    def get_instance(self, context):
        try:
            product = ProductModel.objects.get(pk=context['product'])
        except ProductModel.DoesNotExist:
            raise serializers.ValidationError("Product does not exist.")
        return {
            'product': product.id,
            'product_name': product.product_name,
            'product_code': product.product_code,
            'description': product.description,
            'unit_price': product.get_price(context.get('request', '')),
            'extra_rows': context.get('extra_rows', []),
            'quantity': context.get('quantity', self.fields['quantity'].default),
            'payments': settings.PAYMENT_PROVIDERS
        }

class IOrderSerializer(serializers.Serializer):
    product = serializers.IntegerField()
    quantity = serializers.IntegerField(default=1, min_value=1)
    extra = serializers.DictField(default={})
    pay_method = serializers.CharField(max_length=255)

    product_instance = 0
    def validate_pay_method(self, value):
        if value not in dict([m.get_choice() for m in cart_modifiers_pool.get_payment_modifiers()]):
            raise serializers.ValidationError("Payment method id wrong.")
        return value

    def validate_product(self, value):
        try:
            self.product_instance = ProductModel.objects.get(pk=value)
        except ProductModel.DoesNotExist:
            raise serializers.ValidationError("Product does not exist.")
        return value

    def get_product_instance(self):
        if self.product_instance:
            return self.product_instance
        else:
            serializers.ValidationError("Try is_valid() first!")
