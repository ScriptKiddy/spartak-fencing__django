from django.conf.urls import url
from .views import OrderListView, OrderCancelView, OrderPrintView

urlpatterns = [
    url(r'^$', OrderListView.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/cancel/$', OrderCancelView.as_view(), name='cancel'),
    url(r'^(?P<pk>[0-9]+)/print/$', OrderPrintView.as_view(), name='print'),
]