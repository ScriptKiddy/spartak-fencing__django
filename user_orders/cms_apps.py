from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class OrdersApphook(CMSApp):
    app_name = "user-orders"
    name = _("User Orders Apphook")
    def get_urls(self, page=None, language=None, **kwargs):
        return ["user_orders.urls"]

apphook_pool.register(OrdersApphook)