# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import redirect
from django.views import View
from django.views.generic import DetailView

from rest_framework.generics import ListAPIView
from rest_framework.response import Response

#from shop.rest.money import JSONRenderer
from rest_framework.renderers import TemplateHTMLRenderer#, JSONRenderer, BrowsableAPIRenderer
from rest_framework.pagination import PageNumberPagination

from wkhtmltopdf.views import PDFTemplateView
from django.core.urlresolvers import reverse

from myshop.models import Order
from myshop.serializers import OrderSerializer, CustomerSerializer

class OrdersPagination(PageNumberPagination):
    page_size = 10

class OrderListView(ListAPIView):
    renderer_classes = (TemplateHTMLRenderer,)#, JSONRenderer, BrowsableAPIRenderer)
    serializer_class = OrderSerializer
    pagination_class = OrdersPagination

    def get_queryset(self):
        queryset = Order.objects.order_by('-created_at').filter(customer=self.request.customer)
        return queryset

    def get_template_names(self):
        if not self.request.GET.get('page',None):
            return ('user_orders/page.html',)
        return ('user_orders/list.html',)

class OrderCancelView(View):
    def get(self, request, *args, **kwargs):
        try:
            order = Order.objects.get(pk=kwargs['pk'])
        except Exception as e:
            return redirect(request.META['HTTP_REFERER'])
        order.cancel_order()
        order.save()
        return redirect(request.META['HTTP_REFERER'])

class OrderCheckoutLink(object):

    def __init__(self, order_id):
        self.order_id = order_id

    def get_absolute_url(self):
        return reverse("shop:order-detail", kwargs={'pk': self.order_id})

class OrderPrintView(PDFTemplateView):
    filename = 'ticket.pdf'
    template_name = 'user_orders/print.html'
    show_content_in_browser = True
    
    def get_context_data(self,  **kwargs):
        context = super(OrderPrintView, self).get_context_data(**kwargs)
        try:
            order = Order.objects.get(pk=kwargs['pk'])
        except Exception as e:
            return redirect(request.META['HTTP_REFERER'])  

        context['order'] = OrderSerializer(order, context={'request': self.request}).data
        context['customer'] = CustomerSerializer(order.customer).data
        context['checkout'] = OrderCheckoutLink(order_id = order.pk)
        return context 
