from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool


class ClassExtension(PageExtension):
    page_class = models.CharField(
        _("Page class"),
        max_length=255,
        default="page"
    )

extension_pool.register(ClassExtension)