from django.contrib import admin
from cms.extensions import PageExtensionAdmin

from .models import ClassExtension


class ClassExtensionAdmin(PageExtensionAdmin):
    pass

admin.site.register(ClassExtension, ClassExtensionAdmin)