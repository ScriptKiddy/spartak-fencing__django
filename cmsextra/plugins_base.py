# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.contrib.admin import StackedInline
from django.db.models import Model
from django.forms import  ModelForm, ModelChoiceField, ChoiceField, widgets, CharField
from django.db.models.fields.related import ManyToOneRel
from django.contrib.admin.sites import site

from filer.fields.image import AdminFileWidget, FilerImageField
from filer.models.imagemodels import Image

from cmsplugin_cascade.plugin_base import CascadePluginBase, create_proxy_model
from cmsplugin_cascade.mixins import ImagePropertyMixin
from cmsplugin_cascade.bootstrap3.image import ImageFormMixin as LinkedImageFormMixin
from cmsplugin_cascade.link.config import LinkPluginBase, LinkForm, LinkElementMixin
from cmsplugin_cascade.models import SortableInlineCascadeElement
from adminsortable2.admin import SortableInlineAdminMixin, CustomInlineFormSet

"""
ImagePluginBase
"""
class ImageFormMixin(object):
    def __init__(self, *args, **kwargs):
        super(ImageFormMixin, self).__init__(*args, **kwargs)
        try:
            self.fields['image_file'].initial = kwargs['instance'].image.pk
        except (AttributeError, KeyError):
            pass
        self.fields['image_file'].widget = AdminFileWidget(ManyToOneRel(FilerImageField, Image, 'file_ptr'), site)

    def clean_glossary(self):
        # TODO: remove this someday
        assert isinstance(self.cleaned_data['glossary'], dict)
        return self.cleaned_data['glossary']

    def clean(self):
        cleaned_data = super(ImageFormMixin, self).clean()
        if self.is_valid() and cleaned_data['image_file']:
            image_data = {'pk': cleaned_data['image_file'].pk, 'model': 'filer.Image'}
            cleaned_data['glossary'].update(image=image_data)
        self.cleaned_data.pop('image_file', None)
        return cleaned_data

class ImagePluginForm(ImageFormMixin, ModelForm):
    image_file = ModelChoiceField(queryset=Image.objects.all(), required=False, label=_("Image"))

class ImagePluginBase(CascadePluginBase):
    module = "cmsextra"
    require_parent = False
    allow_children = False
    render_template = "cmsextra/plugins/image-plugin.html"
    model_mixins = (ImagePropertyMixin,)

    def get_form(self, request, obj=None, **kwargs):
        kwargs.update(form=ImagePluginForm)
        form = super(ImagePluginBase, self).get_form(request, obj, **kwargs)
        return form    

    @classmethod
    def get_identifier(cls, obj):
        identifier = super(ImagePluginBase, cls).get_identifier(obj)
        try:
            content = force_text(obj.image)
        except AttributeError:
            content = _("No Picture")
        return format_html('{0}{1}', identifier, content)

"""
Linked Image Plugin Base
"""
class LinkedImagePluginBase(LinkPluginBase):
    module = "cmsextra"
    require_parent = False
    allow_children = False
    render_template = "cmsextra/plugins/linked-image-plugin.html"
    model_mixins = (ImagePropertyMixin, LinkElementMixin)
    fields = ('image_file',) + LinkPluginBase.fields
    def get_form(self, request, obj=None, **kwargs):
        image_file = ModelChoiceField(queryset=Image.objects.all(), required=False, label=_("Image"))
        Form = type(str('ImageForm'), (LinkedImageFormMixin, getattr(LinkForm, 'get_form_class')(),),
            {'LINK_TYPE_CHOICES': LinkedImageFormMixin.LINK_TYPE_CHOICES, 'image_file': image_file})
        kwargs.update(form=Form)
        return super(LinkedImagePluginBase, self).get_form(request, obj, **kwargs)

    @classmethod
    def get_identifier(cls, obj):
        identifier = super(LinkedImagePluginBase, cls).get_identifier(obj)
        try:
            content = force_text(obj.image)
        except AttributeError:
            content = _("No Image")
        return format_html('{0}{1}', identifier, content)


from django.forms import widgets
from django.contrib.admin import StackedInline
from cmsplugin_cascade.models import SortableInlineCascadeElement
from adminsortable2.admin import SortableInlineAdminMixin, CustomInlineFormSet
from djangocms_text_ckeditor.widgets import TextEditorWidget

class GalleryItemForm(ModelForm):

    image_file = ModelChoiceField(
            queryset=Image.objects.all(), 
            label=_("Image"), 
            required=False)

    title = CharField(
            label=_("Item title"), 
            required=False,
            widget=widgets.TextInput())

    text = CharField(
            label=_("Item text"), 
            required=False,
            widget=TextEditorWidget())

    glossary_field_order = ('title', 'text')
    class Meta:
        exclude = ('glossary',)

    def __init__(self, *args, **kwargs):
        try:
            initial = dict(kwargs['instance'].glossary)
        except (KeyError, AttributeError):
            initial = {}
        initial.update(kwargs.pop('initial', {}))
        for key in self.glossary_field_order:
            self.base_fields[key].initial = initial.get(key)
        try:
            self.base_fields['image_file'].initial = initial['image']['pk']
        except KeyError:
            self.base_fields['image_file'].initial = None
        self.base_fields['image_file'].widget = AdminFileWidget(ManyToOneRel(FilerImageField, Image, 'file_ptr'), site)
        super(GalleryItemForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(GalleryItemForm, self).clean()
        if self.is_valid():
            image_file = self.cleaned_data.pop('image_file', None)
            if image_file:
                image_data = {'pk': image_file.pk, 'model': 'filer.Image'}
                self.instance.glossary.update(image=image_data)
            else:
                self.instance.glossary.pop('image', None)
            for key in self.glossary_field_order:
                self.instance.glossary.update({key: cleaned_data.pop(key, '')})
        return cleaned_data


class GalleryPluginInline(SortableInlineAdminMixin, StackedInline):
    model = SortableInlineCascadeElement
    raw_id_fields = ('image_file',)
    form = GalleryItemForm
    extra = 0
    ordering = ('order',)


class ItemsGalleryPluginBase(CascadePluginBase):
    name = _("Items Gallery")
    module = 'cmsextra'
    require_parent = False
    allow_children = False
    admin_preview = False
    render_template = 'cmsextra/plugins_base/items-gallery.html'
    inlines = (GalleryPluginInline,)

    def render(self, context, instance, placeholder):
        gallery_instances = []
        options = dict(instance.get_complete_glossary())
        for inline_element in instance.sortinline_elements.all():
            try:
                ProxyModel = create_proxy_model('GalleryImage', (ImagePropertyMixin,),
                                                SortableInlineCascadeElement, module=__name__)
                inline_element.__class__ = ProxyModel
                gallery_instances.append(inline_element)
            except (KeyError, AttributeError):
                pass
        context.update(dict(instance=instance, placeholder=placeholder, gallery_instances=gallery_instances))
        return context