from django.utils.translation import ugettext_lazy as _
from django.forms import widgets
from cms.plugin_pool import plugin_pool
from django.utils.encoding import force_text
from django.utils.html import format_html
from cmsplugin_cascade.plugin_base import CascadePluginBase, TransparentContainer
from cmsplugin_cascade.fields import GlossaryField
from cmsplugin_cascade.generic.cms_plugins import SimpleWrapperPlugin
from cmsplugin_cascade.mixins import ImagePropertyMixin
from cmsplugin_cascade.link.config import LinkElementMixin
from cmsplugin_cascade.bootstrap3.image import ImageForm
from djangocms_text_ckeditor.widgets import TextEditorWidget
from .plugins_base import ImagePluginBase, LinkedImagePluginBase, ItemsGalleryPluginBase

from booking.models import Service

class WrapperPlugin(SimpleWrapperPlugin):
    module = "cmsextra"
    name = _("Wrapper")
    require_parent = False
    render_template = 'cmsextra/plugins/wrapper.html'

    css_class = GlossaryField(
        widgets.TextInput(),
        label=_("CSS classes"),
    )

    def get_render_template(self, context, instance, placeholder):
        if instance.glossary.get('tag_type') == 'naked':
            return 'cmsextra/plugins/wrapper-naked.html'
        return 'cmsextra/plugins/wrapper.html'

    @classmethod
    def get_identifier(cls, instance):
        identifier = super(SimpleWrapperPlugin, cls).get_identifier(instance)
        tag_name = dict(cls.TAG_CHOICES).get(instance.glossary.get('tag_type'))
        if tag_name:
            return format_html('{0} {1}', tag_name, identifier)
        return identifier

plugin_pool.register_plugin(WrapperPlugin)

class YoutubePlugin(ImagePluginBase):
    module = "cmsextra"
    name = _("Youtube video")
    fields = ('image_file', 'glossary') 
    model_mixins = (ImagePropertyMixin,)
    embed = GlossaryField(
        widgets.Textarea(),
        label=_("Embed code"),
    )
    render_template = 'cmsextra/plugins/youtube.html'

plugin_pool.register_plugin(YoutubePlugin)

class LinkedImagePlugin(LinkedImagePluginBase):
    name = _("Linked Image")
    module = "cmsextra"
    model_mixins = (ImagePropertyMixin, LinkElementMixin)

plugin_pool.register_plugin(LinkedImagePlugin)

class StaticMapPlugin(TransparentContainer, CascadePluginBase):
    module = "cmsextra"
    name = _("StaticMap")
    parent_classes = None
    require_parent = False
    allow_children = True
    alien_child_classes = True
    render_template = "cmsextra/plugins/static-map.html"
plugin_pool.register_plugin(StaticMapPlugin)

class ItemGalleryPlugin(ItemsGalleryPluginBase):
    parent_classes = None
plugin_pool.register_plugin(ItemGalleryPlugin)

"""
COACHS SLIDER
"""
class CoachsSliderPlugin(CascadePluginBase):
    module = "cmsextra"
    name = _("Coachs slider")
    parent_classes = None
    require_parent = False
    allow_children = True
    child_classes = ('CoachsSliderItemPlugin',)
    render_template = "cmsextra/plugins/coachs-slider.html"
plugin_pool.register_plugin(CoachsSliderPlugin)

class CoachsSliderItemPlugin( CascadePluginBase):
    module = "cmsextra"
    name = _("Coachs slider item")
    parent_classes = ('CoachsSliderPlugin',)
    require_parent = True
    allow_children = False
    model_mixins = (ImagePropertyMixin,)
    form = ImageForm
    render_template = "cmsextra/plugins/coachs-slider-item.html"
    change_form_template = "cmsextra/admin/change_form.html"
    fields = ('image_file', 'glossary',)

    title = GlossaryField(
        widgets.TextInput(),
        label=_('Title'),
    )

    text = GlossaryField(
        widgets.Textarea(),
        label=_('Text'),
    )
plugin_pool.register_plugin(CoachsSliderItemPlugin)

"""
SPORTSMAN SLIDER
"""
class SportsmanSliderPlugin( CascadePluginBase ):
    module = "cmsextra"
    name = _("Sportsman slider")
    parent_classes = None
    require_parent = False
    allow_children = True
    child_classes = ('SportsmanSliderItemPlugin',)
    render_template = "cmsextra/plugins/sportsman-slider.html"
plugin_pool.register_plugin(SportsmanSliderPlugin)

class SportsmanSliderItemPlugin( CascadePluginBase ):
    module = "cmsextra"
    name = _("Sportsman slider item")
    parent_classes = ('SportsmanSliderPlugin',)
    require_parent = True
    allow_children = False
    model_mixins = (ImagePropertyMixin,)
    form = ImageForm
    render_template = "cmsextra/plugins/sportsman-slider-item.html"
    change_form_template = "cmsextra/admin/change_form.html"
    fields = ('image_file', 'glossary',)

    title = GlossaryField(
        widgets.TextInput(),
        label=_('Title'),
    )

    text = GlossaryField(
        widgets.Textarea(),
        label=_('Text'),
    )
plugin_pool.register_plugin(SportsmanSliderItemPlugin)

"""
CAMP SLIDER
"""
class CampSliderPlugin( CascadePluginBase ):
    module = "cmsextra"
    name = _("Camp slider")
    parent_classes = None
    require_parent = False
    allow_children = True
    child_classes = ('CampSliderItemPlugin',)
    render_template = "cmsextra/plugins/camp-slider.html"
plugin_pool.register_plugin(CampSliderPlugin)

class CampSliderItemPlugin( CascadePluginBase ):
    module = "cmsextra"
    name = _("Camp slider item")
    parent_classes = ('CampSliderPlugin',)
    require_parent = True
    allow_children = False
    model_mixins = (ImagePropertyMixin,)
    form = ImageForm
    render_template = "cmsextra/plugins/camp-slider-item.html"
    change_form_template = "cmsextra/admin/change_form.html"
    fields = ('image_file', 'glossary',)

    title = GlossaryField(
        widgets.TextInput(),
        label=_('Title'),
    )

    text = GlossaryField(
        widgets.Textarea(),
        label=_('Text'),
        
    )
plugin_pool.register_plugin(CampSliderItemPlugin)