from django.apps import AppConfig


class EntityPluginsConfig(AppConfig):
    name = 'entity_plugins'
