
from django.utils.translation import ugettext_lazy as _
from django.forms import ChoiceField, ModelForm
from django.db.models import Model
from django.forms import widgets
from django.contrib.admin import StackedInline
from cmsplugin_cascade.models import SortableInlineCascadeElement
from cmsplugin_cascade.plugin_base import CascadePluginBase
from adminsortable2.admin import SortableInlineAdminMixin, CustomInlineFormSet

"""
Entity Plugin Base
"""
class EntityPluginForm(ModelForm):
    entity  = ChoiceField()

    class Meta:
        exclude = ('glossary',)
        
    def __init__(self, *args, **kwargs):

        choices = [(0,'')]
        choices.extend( ([ (o.id, str(o)) for o in self.entity_model.objects.all()]) )
        self.base_fields['entity'] = ChoiceField(widget=widgets.Select, choices=choices)

        try:
            initial = dict(kwargs['instance'].glossary)
        except (KeyError, AttributeError):
            initial = {}

        initial.update(kwargs.pop('initial', {}))
        try:
            self.base_fields['entity'].initial = initial['entity']['pk']
        except KeyError:
            self.base_fields['entity'].initial = None

        super(EntityPluginForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(EntityPluginForm, self).clean()
        if self.is_valid():
            entity_pk = self.cleaned_data.pop('entity', None)
            if entity_pk:
                entity_data = {'pk': entity_pk}
                if cleaned_data['glossary'] == None:
                    cleaned_data['glossary']= {'entity':entity_data}
                else:
                    cleaned_data['glossary'].update(entity=entity_data)
        return cleaned_data

class EntityPluginBase(CascadePluginBase):
    module = "entity"
    require_parent = False
    allow_children = False
    #name = "Entity view"
    render_template = "entity_plugins/entity.html"

    entity_model = Model
    context_variable = 'entity'


    def get_form(self, request, obj=None, **kwargs):
        kwargs.update(form=EntityPluginForm)
        form = super(EntityPluginBase, self).get_form(request, obj, **kwargs)
        form.entity_model = self.entity_model
        return form

    def render(self, context, instance, placeholder):
        try:
            entity_id = instance.glossary['entity']['pk']
        except KeyError:
            pass
        else:
            if int(entity_id): 
                try:
                    context[self.context_variable] = self.entity_model.objects.get(pk=entity_id)
                except Exception as e:
                    pass
        return context
"""
Multiple Entity Plugin base
"""
class EntityListPluginForm(ModelForm):
    entity = ChoiceField()

    class Meta:
        exclude = ('glossary', )

    def __init__(self, *args, **kwargs):
        entity_model = kwargs.pop('entity_model')
        choices = [(0, '')]
        choices.extend(([(o.id, str(o)) for o in entity_model.objects.all()]))
        self.base_fields['entity'] = ChoiceField(
            widget=widgets.Select, choices=choices)

        try:
            initial = dict(kwargs['instance'].glossary)
        except (KeyError, AttributeError):
            initial = {}

        initial.update(kwargs.pop('initial', {}))
        try:
            self.base_fields['entity'].initial = initial['entity']['pk']
        except KeyError:
            self.base_fields['entity'].initial = None
        super(EntityListPluginForm, self).__init__(*args, **kwargs)

    def clean(self):

        cleaned_data = super(EntityListPluginForm, self).clean()
        if self.is_valid():
            entity_pk = self.cleaned_data.pop('entity', None)
            if entity_pk:
                entity_data = {'pk': entity_pk}
                self.instance.glossary.update(entity=entity_data)
        return cleaned_data

class EntityListPluginFormset(CustomInlineFormSet):

        def get_form_kwargs(self, index):
            kwargs = super(EntityListPluginFormset, self).get_form_kwargs(index)
            kwargs.update({'entity_model': self.entity_model})
            return kwargs

def TemplateAttributeInline(entity_model):

    class EntityListInline(SortableInlineAdminMixin, StackedInline):
        model = SortableInlineCascadeElement
        raw_id_fields = ('entity',)
        #form = EntityPluginForm2
        #formset = MyFormSet
        extra = 1
        ordering = ('order',)
        verbose_name = _("Inline")
        verbose_name_plural = _("Inlines")

        def get_formset(self, request, obj=None, **kwargs):
            self.form = EntityListPluginForm
            kwargs.update(formset=EntityListPluginFormset)

            formset = super(EntityListInline, self).get_formset(request, obj, **kwargs)
            formset.entity_model = entity_model
            return formset
    return EntityListInline 

class EntityListPluginBase(CascadePluginBase):
    module = "entity"
    require_parent = False
    allow_children = False
    inlines = ()
    render_template = "entity_plugins/entity-list.html"
    entity_model = Model
    context_variable = 'entity'

    def __init__(self, model=None, admin_site=None, glossary_fields=None):
        super(EntityListPluginBase, self).__init__(model, admin_site)
        self.inlines = ( TemplateAttributeInline(self.entity_model), )

    def render(self, context, instance, placeholder):
        entity_ids = []
        for instance in instance.sortinline_elements.all():
            try:
                if int(instance.glossary['entity']['pk']):
                    entity_ids.append(instance.glossary['entity']['pk'])
            except KeyError:
                pass

        try:
            context[self.context_variable] = self.entity_model.objects.filter(pk__in=entity_ids)
        except Exception as e:
            pass
        return context
