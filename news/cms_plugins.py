from django.utils.translation import ugettext_lazy as _
from django.forms import widgets
from cms.plugin_pool import plugin_pool
from cmsplugin_cascade.fields import GlossaryField
from cmsplugin_cascade.plugin_base import CascadePluginBase
from .models import Article
from .serializers import ArticleSerializer

class LatestNewsPlugin(CascadePluginBase):
    module = "news"
    name = _("Latest news")
    require_parent = False
    allow_children = False
    render_template = 'news/plugin-latest-news.html'
    cache = False
    number  = GlossaryField(
        widgets.Select(choices=[ ('6', '6'), ('12', '12'), ('18', '18') ]),
        label=_("Number of articles"),
    )

    def render(self, context, instance, placeholder):
        context = super(LatestNewsPlugin, self).render(context, instance, placeholder)
        num = int(instance.glossary['number']);
        try:
            #Ради того что бы избежать дублирования шаблонов, работает только с сериализаторами
            serializer = ArticleSerializer(Article.objects.all().order_by('-id')[:num], many=True)
            context['results'] = serializer.data
        except Exception as e:
            pass
        return context
plugin_pool.register_plugin(LatestNewsPlugin)