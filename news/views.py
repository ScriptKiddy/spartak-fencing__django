# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.template.loader import get_template
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.renderers import TemplateHTMLRenderer#, JSONRenderer, BrowsableAPIRenderer
from django.views.generic import DetailView

from .models import Article
from .serializers import ArticleSerializer

class ArticlesPagination(PageNumberPagination):
    page_size = 6

class ArticlesListView(ListAPIView):
    renderer_classes = (TemplateHTMLRenderer,)# JSONRenderer, BrowsableAPIRenderer)
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    pagination_class = ArticlesPagination
    def get_template_names(self):
        if not self.request.GET.get('page',None):
            return ('news/page-news.html',)
        return ('news/articles-list.html',)

class ArticleDetailView(RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer,)
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    lookup_field = 'slug'
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return Response({'article': self.object}, template_name='news/page-news-detail.html')