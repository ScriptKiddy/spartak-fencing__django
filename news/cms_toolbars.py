# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.utils.module_loading import import_string
from django.utils.translation import ugettext_lazy as _

from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar

class NewsToolbar(CMSToolbar):
    def populate(self):
        menu = self.toolbar.get_or_create_menu('news', _("News"))

        url = reverse('admin:news_article_changelist')
        menu.add_sideframe_item(_('Articles list'), url=url)

        url = reverse('admin:news_article_add')
        menu.add_modal_item(_('Add article'), url=url)


toolbar_pool.register(NewsToolbar)  