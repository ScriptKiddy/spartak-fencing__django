# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import ImageField, DateTimeField
from easy_thumbnails.templatetags.thumbnail import thumbnail_url
from .models import Article

class ThumbnailSerializer(ImageField):

    def to_representation(self, instance):
        return thumbnail_url(instance, 'half-screen')

class ArticleSerializer(ModelSerializer):
    thumb = ThumbnailSerializer(source="image", read_only=True)
    created = DateTimeField(format="%d.%m.%y", input_formats=None)
    class Meta:
        model = Article
        fields = ('id', 'name', 'description', 'text', 'image', 'thumb', 'embed', 'slug', 'created')
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
