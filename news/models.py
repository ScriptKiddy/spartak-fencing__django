from django.utils.translation import ugettext_lazy as _
from django.db import models
from datetime import date
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.image import FilerImageField
from autoslug import AutoSlugField

class Article(models.Model):
    
    name = models.CharField(
        _("Title"),
        max_length=255,
    )

    description = HTMLField(
        verbose_name=_("Article teaser"),
        configuration='CKEDITOR_SETTINGS_DESCRIPTION',
    )

    text = HTMLField(
        verbose_name=_("Article body"),
        blank=True, 
        null=True, 
    )

    image = FilerImageField(
        verbose_name=('Image'), 
        blank=True, 
        null=True,                         
        on_delete=models.SET_NULL,
        related_name='article_image')

    embed = models.TextField(
            _("Youtube embed code"),
            blank = True,
            null = True)
    
    slug = AutoSlugField(populate_from='name', unique=True, blank=True, always_update=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, db_index=True)


    def __str__(self):
        return self.name     
