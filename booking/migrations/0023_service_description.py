# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-10-22 01:39
from __future__ import unicode_literals

from django.db import migrations
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0022_auto_20171021_2345'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='description',
            field=djangocms_text_ckeditor.fields.HTMLField(blank=True, null=True, verbose_name='Description'),
        ),
    ]
