# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-10-17 15:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0019_auto_20170923_1656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myshop.Customer', verbose_name='Customer'),
        ),
        migrations.AlterField(
            model_name='entry',
            name='event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='booking.Event', verbose_name='Event'),
        ),
        migrations.AlterField(
            model_name='event',
            name='name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='event',
            name='service',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='booking.Service', verbose_name='Service'),
        ),
        migrations.AlterField(
            model_name='event',
            name='spotsFree',
            field=models.IntegerField(default=1, null=True, verbose_name='Spots free'),
        ),
        migrations.AlterField(
            model_name='event',
            name='spotsTotal',
            field=models.IntegerField(default=1, null=True, verbose_name='Spots total'),
        ),
        migrations.AlterField(
            model_name='event',
            name='venue',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='booking.Venue', verbose_name='Venue'),
        ),
        migrations.AlterField(
            model_name='service',
            name='default_venue',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='booking.Venue', verbose_name='Default venue'),
        ),
        migrations.AlterField(
            model_name='service',
            name='group',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='booking.ServiceGroup', verbose_name='Service Group'),
        ),
        migrations.AlterField(
            model_name='servicegroup',
            name='name',
            field=models.CharField(max_length=250, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='venue',
            name='name',
            field=models.CharField(max_length=250, verbose_name='Name'),
        ),
    ]
