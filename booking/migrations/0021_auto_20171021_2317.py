# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-10-21 23:17
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0020_auto_20171017_1542'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='entry',
            options={'verbose_name': 'Entry', 'verbose_name_plural': 'Entries'},
        ),
        migrations.AlterModelOptions(
            name='event',
            options={'verbose_name': 'Event', 'verbose_name_plural': 'Events'},
        ),
        migrations.AlterModelOptions(
            name='servicegroup',
            options={'verbose_name': 'Service group', 'verbose_name_plural': 'Service groups'},
        ),
        migrations.AlterModelOptions(
            name='venue',
            options={'verbose_name': 'Venue', 'verbose_name_plural': 'Venues'},
        ),
    ]
