# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import Service, Event, Entry, Venue

class EventInfoRow(serializers.Serializer):
    label = serializers.CharField(
        read_only=True,
    )
    value = serializers.CharField(
        read_only=True,
    )

class ServiceSerializer(ModelSerializer):
    name = serializers.SerializerMethodField(read_only=True,)

    def get_name(self, obj):
        return obj.product_name;

    class Meta:
        model = Service
        fields = ('id', 'group', 'name', 'unit_price', 'default_venue', 'color')


class EventSerializer(ModelSerializer):
    service_obj = ServiceSerializer(source='service', many=False, read_only=True)
    venue_name = serializers.SlugRelatedField(
        source='venue',
        many=False,
        read_only=True,
        slug_field='name'
     )
    class Meta:
        model = Event
        fields = ('id', 'name', 'service', 'service_obj', 'start', 'end', 'venue', 'venue_name', 'description', 'spotsTotal', 'spotsFree')
        extra_kwargs = {'venue': {'allow_null': True}}
        

    def validate(self, data):
        """
        Устанавливат venue=event.service.default_venue в случае если передан null
        """
        if data['venue'] == None:     
            service = Service.objects.get(pk=data['service'])
            if service.default_venue:
                data['venue'] =  service.default_venue
            else:
                raise serializers.ValidationError(_("Cant identify event venue."))

        """
        Проверяет пересечения по времени
        """
        if data['start'] == data['end'] or data['start'] > data['end']:
            raise serializers.ValidationError(_("Invalid event time"))

        if Event.objects.filter(venue=data['venue']).filter((Q(start__gte=data['start']) & Q(start__lte=data['end'])) | (Q(start__lte=data['start']) & Q(end__gte=data['start']))).count():
            raise serializers.ValidationError( "{} {}-{} {}".format(
                data['start'].strftime("%d/%m/%y"),
                data['start'].strftime("%H:%M"),
                data['end'].strftime("%H:%M"),
                _("Selected time is busy.") ))

        return super(EventSerializer, self).validate(data)


    def validate_title(self, value):
        """
        Check that the blog post is about Django.
        """
        if 'django' not in value.lower():
            raise serializers.ValidationError("Blog post is not about Django")
        return value


class EntrySerializer(ModelSerializer):
    class Meta:
        model = Entry
        fields = ('id', 'event', 'customer')

class VenueSerializer(ModelSerializer):
    class Meta:
        model = Venue
        fields = ('id', 'name')

