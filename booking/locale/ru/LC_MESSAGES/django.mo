��    -      �  =   �      �  	   �  	   �     �  	                  (     4     9     A     \     b     k     y     �     �     �     �  
   �     �     �     �  
   �     �     �     �                    /     7     E     S     b     k  
   y     �  
   �     �  
   �     �     �     �     �  �  �     �     �       #   /     S     n     �  
   �     �  ;   �     �     �  (   		     2	     C	     P	     l	     y	     �	     �	     �	     �	     �	     �	  2   �	     2
     ;
     B
  *   Q
  !   |
     �
     �
     �
     �
     �
  0   �
          9     B     U     f     y     �     �                 '   ,      "   
                                 -      	                                        +      %            (         !       #                              *                 )   &       $    Add entry Add event Add service Add venue Advanced options Bad request Block title Blue Booking Cant identify event venue. Color Customer Default venue Description Entries Entries list Entry Event Event name Events Events list Green Group name Inherit Invalid event time Price for this product Product code Red Selected time is busy. Service Service Group Service group Service groups Services Services list Spots free Spots total Unit price Venue Venue name Venues Venues list date-time end date-time start Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-22 00:07+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Добавить запись Добавить событие добавить услугу Добавить помещение Дополнительно Плохой запрос Заголовок Синий Услуги Не удалось определить помещение Цвет Заказчик Площадка по-умолчанию Описание Записи Список записей Записи События Заголовок События Список событий Зеленый Название унаследовано Неправильное время события Цена Код Красный Выбранное время занято Поставщик событий Группы Услуга Услуга Услуги Список услуг Количество свободных мест Количество мест Цена Помещение Название Помещения Список помещений Время завершения Время начала 