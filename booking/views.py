# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime
from django.utils.translation import ugettext_lazy as _
from rest_framework.generics import RetrieveAPIView, ListAPIView, GenericAPIView, CreateAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import  status
from iorder.views import IOrderView
from .serializers import EventSerializer, ServiceSerializer, EventInfoRow
from .models import Service, Event

class CheckIn(IOrderView):
    """
    Перед созданием заказа мы должны:
        1. создать событие на основе данных из cart_item.extra['event_data']
        2. Записать ID созданного события в cart_item.extra['event_data']
    """
    def post(self, request, *args, **kwargs):
        try:
            event_data = request.data['extra'].pop('event_data')
        except Exception as e:
            return super(CheckIn, self).post(request, *args, **kwargs)

        serializer = EventSerializer(data=event_data)
        serializer.is_valid(raise_exception=True)
        event = serializer.save()           
        serializer.instance.create_entry(request.customer)

        """
        Дополнительная информация в OrderItem
        """
        request.data['extra']['info'] = [
            EventInfoRow({'label':'Дата', 'value': event.start.strftime("%d/%m/%y")}).data,
            EventInfoRow({'label':'Время', 'value': '{} - {}'.format(event.start.strftime("%H:%M"), event.end.strftime("%H:%M"))}).data,
            #EventInfoRow({'label':'Количество часов', 'value': event.end.hour -  event.start.hour }).data,
            #EventInfoRow({'label':'Цена одного часа', 'value': str(serializer.instance.service.get_price(request))}).data
        ]
        request.data['extra']['event_id'] = event.pk
        #request.data['extra']['event_start'] = event.start
        #request.data['extra']['event_start'] = event.start
        return super(CheckIn, self).post(request, *args, **kwargs)
        
    """
    Дополнительная информация в форму заказа
    """
    def retrieve_data(self, request, *args, **kwargs):
        data = super(CheckIn, self).retrieve_data(request, *args, **kwargs)
        data['extra_rows'].append( "<p>Количество часов: <strong>{}</strong></p>".format(request.GET.get('quantity', 1)) )
        return data

class ServiceList(ListModelMixin, GenericAPIView):
    serializer_class = ServiceSerializer
    queryset = Service.objects.all()

    def get_queryset(self):   
        service = self.request.data.get('service', [])
        if not service:
            return super(ServiceList, self).get_queryset()
        return Service.objects.filter(pk__in=service)

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
        
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
"""
class EventsList(ListAPIView):
    serializer_class = EventSerializer
    def get_queryset(self):
        service = self.request.query_params.get('service[]', [])
        raise Exception(service)
        queryset = Event.objects.filter(service__in=service)
        start = self.request.query_params.get('start', None)
        if start is not None :
            queryset = queryset.filter(start__gte=datetime.strptime(start,'"%Y-%m-%dT%H:%M:%S.%fZ"'))        
        end = self.request.query_params.get('end', None)
        if start is not None :
            queryset = queryset.filter(start__lte=datetime.strptime(end,'"%Y-%m-%dT%H:%M:%S.%fZ"')) 
        return queryset
"""
class EventsList(ListModelMixin, GenericAPIView):
    serializer_class = EventSerializer

    def get_queryset(self):
        queryset = Event.objects.all()

        #Фильтр по сервису
        service = self.request.data.get('service', None)
        if service is not None:
            queryset = queryset.filter(service__in=service)


        #Фильтр по площадке
        venue = self.request.data.get('venue', None)
        if venue is not None:
            queryset = queryset.filter(venue__in=venue)

        #Фильтр по датам
        start = self.request.data.get('start', None)
        if start is not None:
            queryset = queryset.filter(start__gte=datetime.strptime(start,'%Y-%m-%dT%H:%M:%S.%fZ'))

        end = self.request.data.get('end')
        if end is not None:
            queryset = queryset.filter(end__lte=datetime.strptime(end,'%Y-%m-%dT%H:%M:%S.%fZ'))

        return queryset

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

class EventsCreate(CreateAPIView):
    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def get_serializer(self, instance=None, data=None, many=False, partial=False):
        return super(EventsCreate, self).get_serializer(instance=instance, data=data, many=True, partial=partial)

class EventsDestroy(APIView):
    def post(self, request, *args, **kwargs):
        try:
            events = Event.objects.filter(pk__in=request.data)
        except Exception as e:
            return Response({'error':_("Bad request")}, status=status.HTTP_400_BAD_REQUEST)
        events.delete()
        return Response({'success': "ok"}, status=status.HTTP_200_OK)    

