
from django.utils.translation import ugettext_lazy as _
from django.db import models
from datetime import date
from shop import deferred
from djangocms_text_ckeditor.fields import HTMLField
from shop.money.fields import MoneyField
from myshop.models import Product, Customer

class ServiceGroup(models.Model):
    name = models.CharField(
        _("Group name"),
        max_length=250)

    def __str__(self):
        return self.name  

    class Meta:
        verbose_name = _('Service group')
        verbose_name_plural = _('Service groups')

class Venue(models.Model):
    name = models.CharField(
        _("Venue name"),
        max_length=250)

    def __str__(self):
        return self.name 
    class Meta:
        verbose_name = _('Venue')
        verbose_name_plural = _('Venues')

class Service(Product):
    COLORS = (
        ('inherit', _('Inherit')),
        ('#eb3f3f', _('Red')),
        ('#3fbd3c', _('Green')),
        ('#0099dc', _('Blue')),
    )

    group = models.ForeignKey(
        ServiceGroup,
        verbose_name=_("Service Group"))

    default_venue = models.ForeignKey(
        Venue, 
        verbose_name=_("Default venue"),
        null=True, 
        blank=True)

    unit_price = MoneyField(
        _("Unit price"),
        decimal_places=0,
        help_text=_("Price for this product"),
    )

    description = HTMLField(
        verbose_name=_("Description"),
        configuration='CKEDITOR_SETTINGS_DESCRIPTION',
        null=True,
        blank=True
    )
    
    product_code = models.CharField(
        _("Product code"),
        max_length=255,
        null=True,
    )

    color = models.CharField(
        _("Color"),
        max_length=255,
        choices=COLORS,
        default="inherit"
    )

    class Meta:
        app_label = 'booking'
        ordering = ('order',)
        verbose_name = _("Service")
        verbose_name_plural = _("Services")

    def save(self, *args, **kwargs):
        if not self.pk:
            self.product_code = "S{}".format( Product.objects.latest('pk').pk+1)
        super(Service, self).save(*args, **kwargs)

    def get_price(self, request):
        return self.unit_price

    def get_absolute_url(self):
        return ''

class Event(models.Model):
    name = models.CharField(
            _("Event name"),
            max_length=255, 
            null=True, 
            blank=True)

    description = HTMLField(
        verbose_name=_("Description"),
        configuration='CKEDITOR_SETTINGS_DESCRIPTION',
        null=True,
        blank=True
    )

    service = models.ForeignKey(
            Service, 
            verbose_name=_("Service"),
            null=True)
    
    spotsTotal = models.IntegerField(
            _("Spots total"),
            null=True, 
            default=1)

    spotsFree = models.IntegerField(
            _("Spots free"),
            null=True, 
            default=1)

    start = models.DateTimeField(
        _('date-time start'))

    end = models.DateTimeField(
        _('date-time end'))

    venue = models.ForeignKey(
            Venue,
            verbose_name=_("Venue")) 

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, db_index=True)

    def create_entry(self, customer):
        if self.spotsFree:
            entry = Entry()
            entry.customer = customer
            entry.event = self
            entry.save()

            self.spotsFree -= 1
            return entry.pk

    def __str__(self):
        return self.service.product_name + ' ' + self.start.strftime("%d/%m/%y")

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')

class Entry(models.Model):
    customer = models.ForeignKey(
            Customer,
            verbose_name=_('Customer'))#deferred.ForeignKey('BaseCustomer', verbose_name="Customer", related_name='entries')
    
    event = models.ForeignKey(
            Event,
            verbose_name=_('Event'))

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        verbose_name = _('Entry')
        verbose_name_plural = _("Entries")
