# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.utils.module_loading import import_string
from django.utils.translation import ugettext_lazy as _

from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.toolbar.items import Break, SubMenu

class BookingToolbar(CMSToolbar):
    def populate(self):
        menu = self.toolbar.get_or_create_menu('booking', _("Booking"))

        service_menu = menu.get_or_create_menu(
            'service-menu',
            _('Services'),
        )

        url = reverse('admin:booking_service_changelist')
        service_menu.add_sideframe_item(_('Services list'), url=url)

        url = reverse('admin:booking_service_add')
        service_menu.add_modal_item(_('Add service'), url=url)

        venue_menu = menu.get_or_create_menu(
            'venue-menu',
            _('Venues'),
        )

        url = reverse('admin:booking_venue_changelist')
        venue_menu.add_sideframe_item(_('Venues list'), url=url)

        url = reverse('admin:booking_venue_add')
        venue_menu.add_modal_item(_('Add venue'), url=url)

        event_menu = menu.get_or_create_menu(
            'event-menu',
            _('Events'),
        )

        url = reverse('admin:booking_event_changelist')
        event_menu.add_sideframe_item(_('Events list'), url=url)

        url = reverse('admin:booking_event_add')
        event_menu.add_modal_item(_('Add event'), url=url)

        entry_menu = menu.get_or_create_menu(
            'entry-menu',
            _('Entries'),
        )

        url = reverse('admin:booking_entry_changelist')
        entry_menu.add_sideframe_item(_('Entries list'), url=url)

        url = reverse('admin:booking_entry_add')
        entry_menu.add_modal_item(_('Add entry'), url=url)


toolbar_pool.register(BookingToolbar)  