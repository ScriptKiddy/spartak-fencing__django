from django.utils.translation import ugettext_lazy as _
from django.forms import widgets
from cms.plugin_pool import plugin_pool
from cmsplugin_cascade.fields import GlossaryField
from cmsplugin_cascade.plugin_base import CascadePluginBase
from entity_plugins.plugins_base import EntityPluginBase, EntityListPluginBase
from .models import ServiceGroup, Service
from .serializers import ServiceSerializer

"""
class BookingPlugin(EntityPluginMixin, CascadePluginBase):
    module = "booking"
    name = "Booking plugin"
    entity_model = ServiceGroup
    render_template = "booking/booking-plugin.html"
    context_variable = 'group'
    cache = False
    title = GlossaryField(
        widgets.TextInput(),
        label=_("Block title")
    )
    def render(self, context, instance, placeholder):
        context = super(BookingPlugin, self).render(context, instance, placeholder);
        context['title'] = instance.glossary['title'];
        try:
            group_id = instance.glossary['entity']['pk']
        except KeyError:
            pass
        else:
            if int(group_id): 
                context['services'] = Service.objects.filter(group=group_id)
        return context  

plugin_pool.register_plugin(BookingPlugin)
"""
class BookingPlugin(EntityListPluginBase):
    module = "booking"
    name = _("Booking plugin")
    entity_model = Service
    render_template = "booking/booking-plugin.html"
    context_variable = 'services'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(BookingPlugin, self).render(context, instance, placeholder)
        context['title'] = instance.glossary['title'];

        entity_ids = []
        for instance in instance.sortinline_elements.all():
            try:
                if int(instance.glossary['entity']['pk']):
                    entity_ids.append(instance.glossary['entity']['pk'])
            except KeyError:
                pass
        context[self.context_variable] = entity_ids;

        return context
plugin_pool.register_plugin(BookingPlugin)


class SchedulePlugin(EntityListPluginBase):
    module = "booking"
    name = _("Schedule plugin")
    entity_model = Service
    render_template = "booking/schedule-plugin.html"
    context_variable = 'services'
    cache = False
    def render(self, context, instance, placeholder):
        context = super(SchedulePlugin, self).render(context, instance, placeholder)
        context['title'] = instance.glossary['title'];

        entity_ids = []
        for instance in instance.sortinline_elements.all():
            try:
                if int(instance.glossary['entity']['pk']):
                    entity_ids.append(instance.glossary['entity']['pk'])
            except KeyError:
                pass
        context[self.context_variable] = entity_ids;
        return context
plugin_pool.register_plugin(SchedulePlugin)
