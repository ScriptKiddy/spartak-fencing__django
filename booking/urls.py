from django.conf.urls import url
from .views import ServiceList, EventsList, CheckIn, EventsCreate, EventsDestroy

urlpatterns = [
    url(r'^services/$', ServiceList.as_view(), name='service-list'),
    url(r'^events/$', EventsList.as_view(), name="event-list"),
    url(r'^events/create$', EventsCreate.as_view(), name="event-create"),
    url(r'^events/destroy$', EventsDestroy.as_view(), name="event-destroy"),
    url(r'^checkin/$', CheckIn.as_view(), name='checkin'),
]