from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import ServiceGroup, Service,  Venue, Event, Entry

admin.site.register(ServiceGroup)
admin.site.register(Venue)
admin.site.register(Event)


class ServiceAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('product_name', 'description', 'group', 'default_venue', 'color')
        }),
        (_('Advanced options'), {
            'fields': ('unit_price', 'active')
        }),
    )

admin.site.register(Service, ServiceAdmin)

class EntryAdmin(admin.ModelAdmin):
    list_display = ('event', 'customer', 'created', 'updated')

admin.site.register(Entry, EntryAdmin)
# Register your models here.
