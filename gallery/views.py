# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.renderers import TemplateHTMLRenderer#, JSONRenderer, BrowsableAPIRenderer

from .models import GalleryItem
from .serializers import GalleryItemSerializer

class GalleryPagination(PageNumberPagination):
    page_size = 16

class GalleryView(ListAPIView):
    renderer_classes = (TemplateHTMLRenderer,)# JSONRenderer, BrowsableAPIRenderer)
    serializer_class = GalleryItemSerializer
    queryset = GalleryItem.objects.all()
    pagination_class = GalleryPagination
    template_name = 'gallery/list.html'