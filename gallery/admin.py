from django.contrib import admin
from django.utils.html import format_html
from easy_thumbnails.templatetags.thumbnail import thumbnail_url
from .models import GalleryItem

class GalleryAdmin(admin.ModelAdmin):

    def image_tag(self, obj):
        return format_html('<img src="{}" width="200px"/>'.format(thumbnail_url(obj.image, 'gallery-thumbnail')))

    list_display = ('image_tag', 'title')
    list_per_page = 30
admin.site.register(GalleryItem, GalleryAdmin)