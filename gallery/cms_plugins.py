from django.utils.translation import ugettext_lazy as _
from django.forms import widgets
from cms.plugin_pool import plugin_pool
from cmsplugin_cascade.fields import GlossaryField
from cmsplugin_cascade.plugin_base import CascadePluginBase
from .models import GalleryItem
from .serializers import GalleryItemSerializer

class GalleryPlugin(CascadePluginBase):
    module = "gallery"
    name = _("Gallery plugin")
    require_parent = False
    allow_children = False
    render_template = 'gallery/plugin.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(GalleryPlugin, self).render(context, instance, placeholder)
        try:
            #Ради того что бы избежать дублирования шаблонов, работает только с сериализаторами
            page_size = 16
            count = len(GalleryItem.objects.all())
            serializer = GalleryItemSerializer(GalleryItem.objects.all().order_by('-id')[:page_size], many=True)
            context['results'] = serializer.data
            if count > page_size:
                context['next'] = '/gallery/?page=2'
        except Exception as e:
            pass
        return context
plugin_pool.register_plugin(GalleryPlugin)