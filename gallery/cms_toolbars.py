# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar

class GalleryToolbar(CMSToolbar):
    def populate(self):
        menu = self.toolbar.get_or_create_menu('gallery', _("Gallery"))

        url = reverse('admin:gallery_galleryitem_changelist')
        menu.add_sideframe_item(_('Items list'), url=url)

        url = reverse('admin:gallery_galleryitem_add')
        menu.add_modal_item(_('Add item'), url=url)


toolbar_pool.register(GalleryToolbar)  