from django.utils.translation import ugettext_lazy as _
from django.db import models
from filer.fields.image import FilerImageField


class GalleryItem(models.Model):

    title = models.CharField(
        _("Title"),
        max_length=255,
    )

    description = models.TextField(
            _("Description"),
            blank = True,
            null = True
    )

    image = FilerImageField(
        verbose_name=('Image'), 
        blank=True, 
        null=True,                         
        on_delete=models.SET_NULL,
        related_name='gallery_image'
    )

    embed = models.TextField(
            _("Youtube embed code"),
            blank = True,
            null = True
    )

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, db_index=True)

    def __str__(self):
        return self.title 

    class Meta:
        verbose_name = _('Gallery item')
        verbose_name_plural = _('Gallery items')