# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import ImageField
from easy_thumbnails.templatetags.thumbnail import thumbnail_url
from .models import GalleryItem

class ThumbnailSerializer(ImageField):

    def to_representation(self, instance):
        return thumbnail_url(instance, 'gallery-thumbnail')

class GalleryItemSerializer(ModelSerializer):
    image = ImageField(use_url=True)
    thumb = ThumbnailSerializer(source="image", read_only=True)
    class Meta:
        model = GalleryItem
        fields = ('id', 'title', 'description', 'image', 'thumb', 'embed', 'created')

