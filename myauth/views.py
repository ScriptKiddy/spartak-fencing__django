# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import TemplateView

from django.contrib.auth import logout
from django.contrib.auth.models import AnonymousUser
from django.utils.translation import ugettext_lazy as _

from django.shortcuts import redirect
from django.template import RequestContext

from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from shop.views import auth
from myshop.models import Customer
from .forms import ProfileEditForm

class ProfileEditView(TemplateView):
    template_name = "myauth/pages/profile.html"
    def get_context_data(self, **kwargs):
        context = super(ProfileEditView, self).get_context_data(**kwargs)
        customer = self.request.customer
        context['profile_edit_form'] = ProfileEditForm(instance=customer)
        return context

class AuthFormsView(GenericAPIView):
    serializer_class = None
    form_class = None

    def post(self, request, *args, **kwargs):

        if request.customer.is_visitor():
            customer = Customer.objects.get_or_create_from_request(request)
        else:
            customer = request.customer

        form = self.form_class(data=request.data, instance=customer)

        if form.is_valid():
            form.save(request=request)
            try:
                msg = form.success_message
            except AttributeError:
                msg = None
            return Response({'success': msg}, status=status.HTTP_200_OK)

        return Response(dict(form.errors), status=status.HTTP_400_BAD_REQUEST)

class LoginView(auth.LoginView):
    pass

class LogoutView(auth.LogoutView):
    
    def get(self, request):
        try:
            request.user.auth_token.delete()
        except:
            pass
        logout(request)
        request.user = AnonymousUser()
        #request.META['HTTP_REFERER']
        return redirect('/')

from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_text

from rest_framework.permissions import AllowAny
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response

from shop.models.customer import CustomerModel
from .serializers import PasswordResetSerializer, PasswordResetConfirmSerializer


class PasswordResetView(GenericAPIView):
    """
    Calls Django Auth PasswordResetForm save method.

    Accepts the following POST parameters: email
    Returns the success/fail message.
    """
    serializer_class = PasswordResetSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        # Create a serializer with request.data
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        # Return the success message with OK HTTP status
        msg = _("Instructions on how to reset the password have been sent to '{email}'.")
        return Response(
            {'success': msg.format(**serializer.data)},
            status=status.HTTP_200_OK
        )

class PasswordResetConfirm(GenericAPIView):
    """
    Password reset e-mail link points onto this view, which when invoked by a GET request renderes
    a HTML page containing a password reset form. This form then can be used to reset the user's
    password using a RESTful POST request.

    Since the URL for this view is part in the email's body text, expose it to the URL patterns as:

    ```
    url(r'^password-reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetConfirm.as_view(), name='password_reset_confirm'),
    ```

    Accepts the following POST parameters: new_password1, new_password2
    Returns the success/fail message.
    """
    renderer_classes = (TemplateHTMLRenderer, JSONRenderer, BrowsableAPIRenderer)
    serializer_class = PasswordResetConfirmSerializer
    permission_classes = (AllowAny,)
    template_name = 'myauth/pages/password-reset.html'
    token_generator = default_token_generator

    def get(self, request, uidb64=None, token=None):
        data = {'uid': uidb64, 'token': token}
        serializer_class = self.get_serializer_class()
        password = get_user_model().objects.make_random_password()
        data.update(new_password1=password, new_password2=password)
        serializer = serializer_class(data=data, context=self.get_serializer_context())
        if not serializer.is_valid():
            return Response({'validlink': False})
        return Response({'validlink': True, 'user_name': force_text(serializer.user)})

    def post(self, request, uidb64=None, token=None):
        data = dict(request.data, uid=uidb64, token=token)
        serializer = self.get_serializer(data=data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        serializer.save()
        return Response({"success": _("Password has been reset with the new password.")})