from classytags.helpers import AsTag, InclusionTag
from classytags.arguments import Argument
from classytags.core import Options
from django import template

register = template.Library()

class btn_ctrl(AsTag):
    def get_value(self, context, name):
        return 'ng-controller="shopAuthBtnCtrl"'

register.tag(btn_ctrl)


class auth_modal_button(InclusionTag):
    template = "myauth/auth_modal/button.html"
    takes_context=False
    options = Options(
        Argument('css'),
        Argument('caption')
    )
    def get_context(self, context, css, caption):
        return {
            'css':css, 
            'caption':caption
        }
register.tag(auth_modal_button)