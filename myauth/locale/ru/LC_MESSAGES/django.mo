��          t      �         �        �     �     �     �     �  %         '     H     X  �  l  �   b  #   0  8   T     �  8   �  8   �  ?     5   W     �     �            
                                      	          A customer with the e-mail address '{email}' already exists.
If you have used this address previously, try to reset the password. Confirm password. Minimum length is 6 characters. New password Passwords do not match Please confirm password Please provide a valid e-mail address Profile data saved successfully. Repeat password Your e-mail address Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-10-22 00:07+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Пользователь с e-mail '{email}' уже зарегистрирован.
Если вы указали правильный адрес, попробуйте восстановить пароль. Подтвердите пароль Минимальная длинна - 6 символов Новый пароль Введенные пороли не совпадают! Пожалуйста подтвердите пароль Пожалуйста укажити правильный e-mail Изменения профиля сохранены! Повторите пароль Ваш e-mail 