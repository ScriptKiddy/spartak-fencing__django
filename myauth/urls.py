# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import url
from rest_auth.views import PasswordChangeView
from .forms import RegisterUserForm, ProfileEditForm
from .views import AuthFormsView, LoginView, LogoutView, PasswordResetView, PasswordResetConfirm

urlpatterns = [
    
    url(r'^login/?$', LoginView.as_view(),
        name='login'),
    url(r'^register/?$', AuthFormsView.as_view(form_class=RegisterUserForm),
        name='register-user'),
    url(r'^logout/?$', LogoutView.as_view(),
        name='logout'),
    url(r'^password/change/?$', PasswordChangeView.as_view(),
        name='password-change'),
    url(r'^password/reset/?$', PasswordResetView.as_view(),
        name='password-reset'),
    url(r'^profile/?$', AuthFormsView.as_view(form_class=ProfileEditForm),
        name='profile-edit'),
    
]
