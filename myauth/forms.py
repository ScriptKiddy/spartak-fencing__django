# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model, authenticate, login
from django.core.exceptions import ValidationError
from django.forms import widgets
from django.shortcuts import render
from django.contrib.sites.shortcuts import get_current_site
from django.template import Context
from django.template.loader import select_template

from djng.forms import fields, NgModelFormMixin, NgFormValidationMixin
from djng.styling.bootstrap3.forms import Bootstrap3ModelForm
from djng.forms.widgets import RadioSelect

from shop.forms.base import UniqueEmailValidationMixin
from myshop.models import Customer

class RegisterUserForm(NgModelFormMixin, NgFormValidationMixin, UniqueEmailValidationMixin, Bootstrap3ModelForm):
    form_name = 'register_user_form'
    scope_prefix = 'form_data'
    field_css_classes = 'input-group has-feedback'

    email = fields.EmailField(label=_("Your e-mail address"))

    preset_password = fields.BooleanField(
        label=_("Preset password"),
        widget=widgets.CheckboxInput(),
        required=False,
        help_text=_("Send a randomly generated password to your e-mail address."))

    password1 = fields.CharField(
        label=_("Choose a password"),
        widget=widgets.PasswordInput,
        min_length=6,
        help_text=_("Minimum length is 6 characters."),
    )

    password2 = fields.CharField(
        label=_("Repeat password"),
        widget=widgets.PasswordInput,
        min_length=6,
        help_text=_("Confirm password."),
    )

    class Meta:
        model = Customer
        fields = ('name', 'surname', 'email', 'phone', 'preset_password', 'password1', 'password2') 

    def __init__(self, data=None, instance=None, *args, **kwargs):
        if data and data.get('preset_password', False):
            pwd_length = max(self.base_fields['password1'].min_length, 8)
            password = get_user_model().objects.make_random_password(pwd_length)
            data['password1'] = data['password2'] = password
        super(RegisterUserForm, self).__init__(data=data, instance=instance, *args, **kwargs)

    def clean(self):
        cleaned_data = super(RegisterUserForm, self).clean()
        # check for matching passwords
        if 'password1' not in self.errors and 'password2' not in self.errors:
            if cleaned_data['password1'] != cleaned_data['password2']:
                msg = _("Passwords do not match")
                raise ValidationError(msg)
        return cleaned_data

    def save(self, request=None, commit=True):
        self.instance.user.is_active = True
        self.instance.user.email = self.cleaned_data['email']
        self.instance.user.set_password(self.cleaned_data['password1'])
        self.instance.recognize_as_registered(request, commit=False)
        customer = super(RegisterUserForm, self).save(commit)
        password = self.cleaned_data['password1']
        self._send_notification(request, customer, password)
        user = authenticate(username=customer.user.username, password=password)
        login(request, user)
        return customer

    def _send_notification(self, request, customer, password):
        user = customer.user
        current_site = get_current_site(request)
        context = Context({
            'site_name': current_site.name,
            'absolute_base_uri': request.build_absolute_uri('/'),
            'email': user.email,
            'password': password,
            'customer': customer,
        })
        subject = "{} {}".format(_("Welcome to"), current_site.name)
        subject = ''.join(subject.splitlines())

        body_txt = select_template(['myauth/email/registration.txt',]).render(context)
        body_html = select_template(['myauth/email/registration.html',]).render(context)

        user.email_user(subject, body_txt, html_message=body_html)

class ProfileEditForm(NgModelFormMixin, NgFormValidationMixin, Bootstrap3ModelForm):
    success_message = _("Profile data saved successfully.")
    form_name = 'profile_edit_form'
    scope_prefix = 'form_data'

    email = fields.EmailField(label=_("Your e-mail address"))

    password1 = fields.CharField(
        required=False,
        label=_("New password"),
        widget=widgets.PasswordInput,
        min_length=6,
        help_text=_("Minimum length is 6 characters."),
    )

    password2 = fields.CharField(
        required=False,
        label=_("Repeat password"),
        widget=widgets.PasswordInput,
        min_length=6,
        help_text=_("Confirm password."),
    )

    class Meta:
        model = Customer
        fields = ('name', 'surname', 'age', 'sex', 'email','phone', 'password1', 'password2')
 
    def __init__(self, data=None, instance=None, *args, **kwargs):
        if not data or not data.get('email', False):
            kwargs.update(initial={
                'email': instance.user.email
            })
        super(ProfileEditForm, self).__init__(data=data, instance=instance, *args, **kwargs)

    def clean_email(self):
        if not self.cleaned_data['email']:
            raise ValidationError(_("Please provide a valid e-mail address"))
        # check for uniqueness of email address
        if self.instance.user.email != self.cleaned_data['email']:
            if Customer.objects.filter(is_active=True, user_email=self.cleaned_data['email']).exists():
                msg = _("A customer with the e-mail address '{email}' already exists.\n"
                        "If you have used this address previously, try to reset the password.")
                raise ValidationError(msg.format(**self.cleaned_data))
        return self.cleaned_data['email']

    def clean(self):
        cleaned_data = super(ProfileEditForm, self).clean()
        # check for matching passwords
        if 'password1' in self.cleaned_data:
            if 'password2' not in self.cleaned_data:
                raise ValidationError(_("Please confirm password"))
            if cleaned_data['password1'] != cleaned_data['password2']:
                msg = _("Passwords do not match")
                raise ValidationError(msg)
        return cleaned_data

    def save(self, request=None, commit=True):
        user = super(ProfileEditForm, self).save(commit)
        if self.cleaned_data['password1']:
            self.instance.user.set_password(self.cleaned_data['password1'])
            if commit:
                user.save()
        return user