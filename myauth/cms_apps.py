
from django.utils.translation import ugettext_lazy as _

from django.conf.urls import url

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .views import ProfileEditView


class MyAuthApphook(CMSApp):
    app_name = "user"
    name = _("MyAuth Apphook")
    def get_urls(self, page=None, language=None, **kwargs):
        return [
            url(r'^$', ProfileEditView.as_view(),
            name='profile'),
        ]  
apphook_pool.register(MyAuthApphook)

