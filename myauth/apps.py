from django.apps import AppConfig

class MyauthConfig(AppConfig):
    name = 'myauth'
    verbose_name = 'Authorisation'
