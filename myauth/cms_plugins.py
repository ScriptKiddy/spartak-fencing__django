from django.utils.translation import ugettext_lazy as _
from cms.plugin_pool import plugin_pool
from cmsplugin_cascade.plugin_base import CascadePluginBase
from .forms import RegisterUserForm

class AuthModalPlugin(CascadePluginBase):
    module = "Auth"
    name = "Auth-Modal Instance"
    require_parent = False
    allow_children = False
    render_template = "myauth/auth_modal/modal.html"
    cache = False
    def render(self, context, instance, placeholder):
        context = super(AuthModalPlugin, self).render(context, instance, placeholder);
        #context['form'] = RegisterUserForm()
        return context  

plugin_pool.register_plugin(AuthModalPlugin)