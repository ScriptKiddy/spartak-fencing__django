from django.utils import timezone
from django.views.generic import TemplateView
from django.conf import settings
from yandex_money.forms import PaymentForm
from yandex_money.models import Payment
from shop.models.order import OrderModel

class RedirectView(TemplateView):
    template_name = 'shop_yandex/redirect_page.html'

    def get_context_data(self, **kwargs):
        try:
            order_id = kwargs.get('order')  
            order = OrderModel.objects.get(pk=order_id)
        except KeyError:
            raise Exception('You need to specify the order id')
        except OrderModel.DoesNotExist:
            raise Exception('Order  "%s" does not exist' % str(order_id))
        payment = None
        try:
            payment = Payment.objects.get(order_number=order.pk, shop_id=settings.YANDEX_MONEY_SHOP_ID)
        except Payment.DoesNotExist:
            payment = Payment()

        payment.order_amount=1234
        payment.order_number=order.pk
        payment.payment_type = 'AC'
        payment.customer_number = order.customer.pk
        payment.cps_email = order.customer.email
        payment.modified = timezone.now()
        payment.save()     

        ctx = super(RedirectView, self).get_context_data(**kwargs)
        ctx['form'] = PaymentForm(instance=payment)
        return ctx