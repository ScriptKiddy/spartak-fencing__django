# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import ValidationError
from shop.models.order import BaseOrder, OrderModel, OrderPayment
from shop.payment.base import PaymentProvider
from django_fsm import transition
from django.urls import reverse_lazy

from yandex_money.forms import PaymentForm
from yandex_money.models import Payment

class YandexPayment(PaymentProvider):
    """
    Provides a payment service for Stripe.
    """
    namespace = 'yandex-payment'
    
    def get_payment_request(self, cart, request):
        order = OrderModel.objects.create_from_cart(cart, request)
        order.populate_from_cart(cart, request)
        if order.total == 0:
            order.no_payment_required()
        else:
            order.awaiting_payment()
        order.save()          
        url = reverse_lazy('user-orders:list')#reverse_lazy('shop-yandex:redirect',  kwargs={'order': order.pk}) 
        js_expression = '$window.location.href="{}";'.format(url)  
        return js_expression
    """  
    def get_payment_request_from_order(self, order, request):
        payment = self.create_payment(order)
        data = self.get_payment_data(payment)
        return { 'url': self.URL, 'data': data }

    def create_payment(self, order):
        payment = Payment()
        payment.order_amount=1234
        payment.order_number=order.pk
        payment.payment_type = 'AC'
        payment.customer_number = order.customer.pk
        payment.cps_email = order.customer.email
        payment.save()     

        return payment
    
    def get_payment_data(self, payment):
        return {
            'shopId': payment.shop_id,
            'scid': payment.scid,
            'sum': payment.order_amount,
            'customerNumber': payment.customer_number,
            'paymentType': payment.payment_type,
            'cps_email': payment.cps_email,
        }
    """
class OrderWorkflowMixin(object):
    TRANSITION_TARGETS = {
        'paid_with_yandex': _("Paid using Yandex"),
    }
 
    def __init__(self, *args, **kwargs):
        if not isinstance(self, BaseOrder):
            raise ImproperlyConfigured('OrderWorkflowMixin is not of type BaseOrder')
        super(OrderWorkflowMixin, self).__init__(*args, **kwargs)

    @transition(field='status', source=['awaiting_payment'], target='paid_with_yandex')
    def add_yandex_payment(self, charge):
        payment = OrderPayment(order=self, transaction_id=charge['id'], payment_method=YandexPayment.namespace)
        assert payment.amount.currency == charge['currency'].upper(), "Currency mismatch"
        payment.amount = payment.amount.__class__(Decimal(charge['amount']) / payment.amount.subunits)
        payment.save()

    def is_fully_paid(self):
        return super(OrderWorkflowMixin, self).is_fully_paid()

    @transition(field='status', source='paid_with_yandex', conditions=[is_fully_paid],
        custom=dict(admin=True, button_name=_("Acknowledge Payment")))
    def acknowledge_yandex_payment(self):
        self.acknowledge_payment()
