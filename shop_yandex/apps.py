from django.apps import AppConfig


class ShopYandexConfig(AppConfig):
    name = 'shop_yandex'
