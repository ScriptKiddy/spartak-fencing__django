from django.conf.urls import url
from .views import RedirectView

urlpatterns = [
     url(r'^(?P<order>[0-9]+)/$', RedirectView.as_view(), name='redirect'),
]