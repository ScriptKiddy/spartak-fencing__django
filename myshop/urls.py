from django.conf.urls import url
from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse_lazy
from .views import OrderDetailView, OrderPurchaseView, OrderCancelView

urlpatterns = [
    url(r'^orders/(?P<pk>[0-9]+)/$', permission_required('myshop.manager', login_url=reverse_lazy('login-page'))(OrderDetailView.as_view()), name='order-detail'),
    url(r'^orders/(?P<pk>[0-9]+)/purchase/$', permission_required('myshop.manager', login_url=reverse_lazy('login-page'))(OrderPurchaseView.as_view()), name='order-purchase'),
    url(r'^orders/(?P<pk>[0-9]+)/cancel/$', permission_required('myshop.manager', login_url=reverse_lazy('login-page'))(OrderCancelView.as_view()), name='order-cancel'),
]