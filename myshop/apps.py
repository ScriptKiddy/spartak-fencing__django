from django.apps import AppConfig

class MyshopConfig(AppConfig):
    name = 'myshop'
    def ready(self):
        from django_fsm.signals import post_transition
        post_transition.connect(my_order_event_notification)


def my_order_event_notification(sender, instance=None, target=None, **kwargs):
    from django.core.files.base import ContentFile

    from wkhtmltopdf.views import PDFTemplateResponse
    from post_office import mail

    from shop.models.notification import EmulateHttpRequest
    from shop.models.order import BaseOrder

    from user_orders.views import OrderCheckoutLink
    from myshop.serializers import CustomerSerializer, OrderSerializer

    if target == 'awaiting_payment':
        if not isinstance(instance, BaseOrder):
            return

        recipient = instance.customer.email
        emulated_request = EmulateHttpRequest(instance.customer, instance.stored_request)
        customer_serializer = CustomerSerializer(instance.customer)
        order_serializer = OrderSerializer(instance, context={'request': emulated_request})
        language = instance.stored_request.get('language')
        
        mail_context = {
            'customer': customer_serializer.data,
            'data': order_serializer.data,
            'ABSOLUTE_BASE_URI': emulated_request.build_absolute_uri().rstrip('/'),
            'render_language': language,
        }

        pdf_context = {}
        pdf_context['order'] = OrderSerializer(instance, context={'request': emulated_request}).data
        pdf_context['customer'] = CustomerSerializer(instance.customer).data
        pdf_context['checkout'] = OrderCheckoutLink(order_id = instance.pk)

        pdf = PDFTemplateResponse(
                request=emulated_request,
                template='user_orders/print.html',
                show_content_in_browser=False,
                header_template=None,
                footer_template=None,
                filename='ticket.pdf',
                context=pdf_context,
                cmd_options={'load-error-handling': 'ignore'})

        attachments = {'ticket.pdf': { 'file': ContentFile(pdf.rendered_content), 'mimetype': 'application/pdf'},}
        mail.send(recipient, template='order_info', context=mail_context,
                  attachments=attachments, render_on_delivery=True)
    
