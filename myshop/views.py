# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from django.shortcuts import redirect
from django.views import View
from django.views.generic import DetailView
from shop.models.order import OrderPayment
from .models import Order
from .models import Order
from .serializers import OrderSerializer, CustomerSerializer

"""
Проверка заказа
"""
class OrderDetailView(DetailView):
    model = Order
    template_name="myshop/orders/detail.html"
    def get(self, request, *args, **kwargs):
        return super(OrderDetailView, self).get(request, *args, **kwargs)


"""
Отменить заказ
"""
class OrderCancelView(View):

    def get(self, request, *args, **kwargs):
        try:
            order = Order.objects.get(pk=kwargs['pk'])
        except Exception as e:
            return redirect(request.META['HTTP_REFERER'])
        if not  order.is_fully_paid():
            order.cancel_order()
            order.save()
        return redirect(request.META['HTTP_REFERER'])

"""
Подтверждение оплаты
"""
class OrderPurchaseView(View):
    def get(self, request, *args, **kwargs):
        try:
            order = Order.objects.get(pk=kwargs['pk'])
        except Exception as e:
            return redirect(request.META['HTTP_REFERER'])
        
        if not  order.is_fully_paid():
            payment = OrderPayment(
                order = order, 
                amount = order.total, 
                transaction_id="'O{}-{}".format(order.pk, datetime.now().strftime("%d/%m/%y_%H:%M")),
                payment_method = 'pay-in-advance'
            )
            payment.save()
            order.amount_paid = order.total
        
        order.acknowledge_payment()
        order.save()
        return redirect(request.META['HTTP_REFERER'])