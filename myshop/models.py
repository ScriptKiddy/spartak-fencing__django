# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from shop.models.defaults.address import ShippingAddress, BillingAddress
from shop.models.defaults.cart import Cart
from shop.models.defaults.cart_item import CartItem 
#from shop.models.defaults.customer import Customer
from shop.models.customer import BaseCustomer
from shop.models.defaults.order import Order 
from shop.models.defaults.order_item import OrderItem
from shop.models.order import OrderPayment
from shop.models.product import BaseProductManager, BaseProduct
from shop.money.fields import MoneyField

class MyshopPermissions(models.Model):
    class Meta:
        managed = False
        permissions = (
            ("manager", "Can purchase/cancel custom orders"),
        )

class Customer(BaseCustomer):
    name = models.CharField(
        _("Name"),
        max_length=255,
        blank=True,
    )

    surname = models.CharField(
        _("Surname"),
        max_length=255,
        blank=True,
    )

    phone = models.CharField(
        _("Phone"),
        max_length=255,
        blank=True,
    )

    number = models.PositiveIntegerField(
        _("Customer Number"),
        null=True,
        default=None,
        unique=True,
    )

    age = models.PositiveIntegerField(
        _("Customer Age"),
        null=True,
        default=None,
        unique=False,
    )

    sex = models.CharField(
        _("Sex"),
        max_length=7,
        choices=( ('male', _('Male')) , ('female', _('Female')) ),
        blank=True, 
    )

    def get_number(self):
        return self.number

    def get_or_assign_number(self):
        if self.number is None:
            aggr = Customer.objects.filter(number__isnull=False).aggregate(models.Max('number'))
            self.number = (aggr['number__max'] or 0) + 1
            self.save()
        return self.get_number()

class Product(BaseProduct):
    product_name = models.CharField(
        _("Product name"),
        max_length=255,
    )
    order = models.PositiveIntegerField(
        _("Sort by"),
        db_index=True,
        default=0
    )
    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    objects = BaseProductManager()

    # filter expression used to lookup for a product item using the Select2 widget
    lookup_fields = ('product_name__icontains',)

    def __str__(self):
        return self.product_name
