# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from shop.models.order import OrderModel
from shop.payment.base import PaymentProvider


class ForwardFundPayment(PaymentProvider):
    """
    Provides a simple prepayment payment provider.
    """
    namespace = 'forward-fund-payment'

    def get_payment_request(self, cart, request):
        order = OrderModel.objects.create_from_cart(cart, request)
        order.populate_from_cart(cart, request)
        order.save()
        if order.total == 0:
            order.no_payment_required()
        else:
            order.awaiting_payment()
        order.save()
        thank_you_url = reverse_lazy('user-orders:list')
        return '$window.location.href="{}";'.format(thank_you_url)