from django.contrib import admin
from django.template.loader import select_template
from django.utils.translation import pgettext_lazy
from django.forms import widgets, ModelForm
from shop.admin.order import PrintOrderAdminMixin, OrderPaymentInline
from shop.admin.defaults.order import OrderAdmin
from .models import Customer, Order, OrderItem
from decimal import Decimal
class MyOrderPaymentInlineForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MyOrderPaymentInlineForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if(instance):
            amount =  instance.amount.quantize(Decimal(1.0))
            self.initial['amount'] = amount



class MyOrderPaymentInline(OrderPaymentInline):
    form = MyOrderPaymentInlineForm
    def has_delete_permission(self, request, obj=None):
        return True


class OrderItemInline(admin.StackedInline):
    model = OrderItem
    extra = 0
    fieldsets = (
      ('Услуга', {
          'fields': (('product_name','quantity','render_as_html_extra',),)
      }),
    )
    readonly_fields = ('product_name', 'quantity', 'render_as_html_extra',)
    template = "myshop/admin/orderitems.html"

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_max_num(self, request, obj=None, **kwargs):
        return self.model.objects.filter(order=obj).count()

    def render_as_html_extra(self, obj):
        item_extra_template = select_template(['myshop/admin/orderitem-product-extra.html',])
        return item_extra_template.render(obj.extra)

    render_as_html_extra.short_description = pgettext_lazy('admin', "Product info")

############################################
# ORDER ADMIN
############################################
class MyOrderAdmin(OrderAdmin):
    list_display = ('get_number', 'customer', 'status_name', 'total', 'created_at',)
    fields = None
    fieldsets = (
        ('Данные о заказе', {
            'fields': ( ('get_number','created_at', 'updated_at'),('status_name', 'get_subtotal', 'get_total') )
        }),
        ('Заказчик', {
            'fields': (('get_customer_name', 'get_customer_email', 'get_customer_phone'),)
        })
    )
    readonly_fields = ('get_number', 'status_name', 'get_total', 'get_subtotal',
                        'get_outstanding_amount', 'created_at', 'updated_at',
                       'render_as_html_extra', 'stored_request', 'get_customer_name', 
                       'get_customer_email', 'get_customer_phone')
    inlines = ( OrderItemInline, MyOrderPaymentInline,)
    change_form_template = "myshop/admin/order.html"
    def get_fields(self, request, obj=None):
        return self.fields

    def get_customer_name(self, obj):
        return "{} {}".format(obj.customer.surname,obj.customer.name)
    get_customer_name.short_description = pgettext_lazy('admin', "Name")

    def get_customer_email(self, obj):
        return obj.customer.email
    get_customer_email.short_description = pgettext_lazy('admin', "E-Mail")   

    def get_customer_phone(self, obj):
        return obj.customer.phone
    get_customer_phone.short_description = pgettext_lazy('admin', "Phone")   

class OrderAdmin(PrintOrderAdminMixin, MyOrderAdmin):
            pass

admin.site.register(Order, OrderAdmin)


###########################################
# CUSTOMERS
###########################################
admin.site.register(Customer)

admin.site.site_header = "Shop administration"