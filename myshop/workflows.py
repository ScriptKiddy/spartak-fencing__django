# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django_fsm import transition

class MyCustomWorkflowMixin(object):

    TRANSITION_TARGETS = {
        'awaiting_payment': _("Awaiting a forward fund payment"),
        'prepayment_deposited': _("Prepayment deposited"),
        'no_payment_required': _("No Payment Required"),
        'order_canceled': _("Order Canceled"),
    }

    def is_fully_paid(self):
        return super(MyCustomWorkflowMixin, self).is_fully_paid()

    def no_open_deposits(self):
        return self.amount_paid == 0

    @transition(field='status', source=['*'], target='prepayment_deposited',
        conditions=[is_fully_paid], permission='myshop.manager')
    def offline_prepayment_fully_deposited(self):
        """
        Signals that the current Order received a payment, which fully covers the requested sum.
        """

    @transition(field='status', source=['*'], target='order_canceled',
        conditions=[no_open_deposits], permission='myshop.manager')
    def customer_cancel_order(self):
        """
        Signals that an Order shall be canceled.
        """

    @transition(field='status', source=['*'], target='order_canceled',
        conditions=[no_open_deposits], permission='myshop.manager')
    def editor_cancel_order(self):
        """
        Signals that an Order shall be canceled.
        """
