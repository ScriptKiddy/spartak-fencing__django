# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import CharField, DateTimeField, ReadOnlyField
from shop.serializers.order import OrderDetailSerializer
from shop.serializers.bases import BaseCustomerSerializer
from .models import Order, Customer

class OrderSerializer(OrderDetailSerializer):
    status_code = CharField(
        source='status',
        read_only=True,
    )
    id = ReadOnlyField()
    created_at = DateTimeField(format="%d.%m.%y %H:%M", input_formats=None)
    updated_at = DateTimeField(format="%d.%m.%y %H:%M", input_formats=None)
    class Meta:
        model = Order 
        exclude = [ 'customer', 'stored_request', '_subtotal', '_total']

class CustomerSerializer(BaseCustomerSerializer):
    class Meta:
        model = Customer
        fields = [ 'name', 'surname', 'email', 'phone', 'age', 'sex']
