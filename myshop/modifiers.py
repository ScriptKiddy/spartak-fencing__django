# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from shop.modifiers.base import PaymentModifier
from .payment import ForwardFundPayment

class PayInAdvanceModifier(PaymentModifier):
    """
    This modifiers has not influence on the cart final. It can be used,
    to enable the customer to pay the products on delivery.
    """
    identifier = 'pay-in-advance'
    payment_provider = ForwardFundPayment()

    def get_choice(self):
        return (self.identifier, _("Pay in advance"))
